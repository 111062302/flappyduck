
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Bullet');
require('./assets/Scripts/BulletManager');
require('./assets/Scripts/Duck');
require('./assets/Scripts/pigBoss');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/pigBoss.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '055a2GqhJNFDobE1/BmcAkf', 'pigBoss');
// Scripts/pigBoss.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Duck_js_1 = require("./Duck.js");
var BulletManager_1 = require("./BulletManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var pigBoss = /** @class */ (function (_super) {
    __extends(pigBoss, _super);
    function pigBoss() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.originX = 0;
        _this.xSpeed = 100;
        _this.bulletManager = null;
        _this.duck = null;
        _this.bulletPrefab = null;
        _this.bulletFrame = null;
        _this.bulletNumer = 9;
        _this.bulletSpeed = 200;
        _this.moveRange = 50;
        return _this;
    }
    pigBoss.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
    };
    pigBoss.prototype.start = function () {
        this.originX = this.node.x;
    };
    pigBoss.prototype.update = function (dt) {
        if (Math.abs(this.node.x - this.originX) >= this.moveRange && Math.abs(this.node.x + this.xSpeed - this.originX) >= Math.abs(this.node.x - this.originX)) {
            this.xSpeed *= -1;
        }
        this.node.x += this.xSpeed * dt;
    };
    pigBoss.prototype.shoot = function () {
        var _this = this;
        var radius = 150;
        var i = 0;
        cc.tween(this.node)
            .call(function () {
            var a = _this.node.x + radius * Math.cos(Math.PI * i / _this.bulletNumer);
            var b = _this.node.y + radius * Math.sin(Math.PI * i / _this.bulletNumer);
            _this.bulletManager.createBullet(cc.v2(a, b), cc.v2(Math.cos(Math.PI * i / _this.bulletNumer), Math.sin(Math.PI * i / _this.bulletNumer)), _this.bulletSpeed, BulletManager_1.BulletType.PlayerBullet, 180 * i / _this.bulletNumer);
            i++;
        })
            .delay(0.1)
            .union()
            .repeat(this.bulletNumer)
            .start();
    };
    __decorate([
        property(BulletManager_1.default)
    ], pigBoss.prototype, "bulletManager", void 0);
    __decorate([
        property(Duck_js_1.default)
    ], pigBoss.prototype, "duck", void 0);
    __decorate([
        property(cc.Prefab)
    ], pigBoss.prototype, "bulletPrefab", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], pigBoss.prototype, "bulletFrame", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "bulletNumer", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "bulletSpeed", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "moveRange", void 0);
    pigBoss = __decorate([
        ccclass
    ], pigBoss);
    return pigBoss;
}(cc.Component));
exports.default = pigBoss;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xccGlnQm9zcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxxQ0FBNEI7QUFDNUIsaURBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXFDLDJCQUFZO0lBQWpEO1FBQUEscUVBdURDO1FBckRXLGFBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIsWUFBTSxHQUFXLEdBQUcsQ0FBQztRQUc3QixtQkFBYSxHQUFrQixJQUFJLENBQUM7UUFFcEMsVUFBSSxHQUFTLElBQUksQ0FBQztRQUVsQixrQkFBWSxHQUFjLElBQUksQ0FBQztRQUUvQixpQkFBVyxHQUFtQixJQUFJLENBQUM7UUFFbkMsaUJBQVcsR0FBVyxDQUFDLENBQUM7UUFFeEIsaUJBQVcsR0FBVyxHQUFHLENBQUM7UUFFMUIsZUFBUyxHQUFXLEVBQUUsQ0FBQzs7SUFxQzNCLENBQUM7SUFsQ0csd0JBQU0sR0FBTjtRQUNJLEVBQUUsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ25ELENBQUM7SUFFRCx1QkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUUvQixDQUFDO0lBRUQsd0JBQU0sR0FBTixVQUFRLEVBQUU7UUFDTixJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM3SSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3JCO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFHcEMsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFBQSxpQkFlQztRQWRHLElBQUksTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDVixFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDZCxJQUFJLENBQUM7WUFDRixJQUFJLENBQUMsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3hFLEtBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsRUFDekgsS0FBSSxDQUFDLFdBQVcsRUFBRSwwQkFBVSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuRixDQUFDLEVBQUUsQ0FBQztRQUNSLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxHQUFHLENBQUM7YUFDVixLQUFLLEVBQUU7YUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQzthQUN4QixLQUFLLEVBQUUsQ0FBQTtJQUNoQixDQUFDO0lBaEREO1FBREMsUUFBUSxDQUFDLHVCQUFhLENBQUM7a0RBQ1k7SUFFcEM7UUFEQyxRQUFRLENBQUMsaUJBQUksQ0FBQzt5Q0FDRztJQUVsQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2lEQUNXO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7Z0RBQ1U7SUFFbkM7UUFEQyxRQUFRO2dEQUNlO0lBRXhCO1FBREMsUUFBUTtnREFDaUI7SUFFMUI7UUFEQyxRQUFROzhDQUNjO0lBbEJOLE9BQU87UUFEM0IsT0FBTztPQUNhLE9BQU8sQ0F1RDNCO0lBQUQsY0FBQztDQXZERCxBQXVEQyxDQXZEb0MsRUFBRSxDQUFDLFNBQVMsR0F1RGhEO2tCQXZEb0IsT0FBTyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBEdWNrIGZyb20gXCIuL0R1Y2suanNcIlxyXG5pbXBvcnQgQnVsbGV0TWFuYWdlciwge0J1bGxldFR5cGV9IGZyb20gXCIuL0J1bGxldE1hbmFnZXJcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgcGlnQm9zcyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgcHJpdmF0ZSBvcmlnaW5YOiBudW1iZXIgPSAwO1xyXG4gICAgcHJpdmF0ZSB4U3BlZWQ6IG51bWJlciA9IDEwMDtcclxuXHJcbiAgICBAcHJvcGVydHkoQnVsbGV0TWFuYWdlcilcclxuICAgIGJ1bGxldE1hbmFnZXI6IEJ1bGxldE1hbmFnZXIgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KER1Y2spXHJcbiAgICBkdWNrOiBEdWNrID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5QcmVmYWIpXHJcbiAgICBidWxsZXRQcmVmYWI6IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXHJcbiAgICBidWxsZXRGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5XHJcbiAgICBidWxsZXROdW1lcjogbnVtYmVyID0gOTtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgYnVsbGV0U3BlZWQ6IG51bWJlciA9IDIwMDtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgbW92ZVJhbmdlOiBudW1iZXIgPSA1MDtcclxuXHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICBjYy5kaXJlY3Rvci5nZXRQaHlzaWNzTWFuYWdlcigpLmVuYWJsZWTCoD3CoHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQgKCkge1xyXG4gICAgICAgIHRoaXMub3JpZ2luWCA9IHRoaXMubm9kZS54O1xyXG4gICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmKE1hdGguYWJzKHRoaXMubm9kZS54LXRoaXMub3JpZ2luWCkgPj0gdGhpcy5tb3ZlUmFuZ2UgJiYgTWF0aC5hYnModGhpcy5ub2RlLngrdGhpcy54U3BlZWQtdGhpcy5vcmlnaW5YKSA+PSBNYXRoLmFicyh0aGlzLm5vZGUueC10aGlzLm9yaWdpblgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMueFNwZWVkICo9IC0xO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5vZGUueCArPSB0aGlzLnhTcGVlZCAqIGR0O1xyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIHNob290ICgpIHtcclxuICAgICAgICBsZXQgcmFkaXVzID0gMTUwO1xyXG4gICAgICAgIGxldCBpID0gMDtcclxuICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXHJcbiAgICAgICAgICAgIC5jYWxsKCgpPT57XHJcbiAgICAgICAgICAgICAgICBsZXQgYSA9IHRoaXMubm9kZS54ICsgcmFkaXVzICogTWF0aC5jb3MoTWF0aC5QSSAqIGkgLyB0aGlzLmJ1bGxldE51bWVyKTtcclxuICAgICAgICAgICAgICAgIGxldCBiID0gdGhpcy5ub2RlLnkgKyByYWRpdXMgKiBNYXRoLnNpbihNYXRoLlBJICogaSAvIHRoaXMuYnVsbGV0TnVtZXIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5idWxsZXRNYW5hZ2VyLmNyZWF0ZUJ1bGxldChjYy52MihhLGIpLCBjYy52MihNYXRoLmNvcyhNYXRoLlBJICogaSAvIHRoaXMuYnVsbGV0TnVtZXIpLCBNYXRoLnNpbihNYXRoLlBJICogaSAvIHRoaXMuYnVsbGV0TnVtZXIpKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYnVsbGV0U3BlZWQsIEJ1bGxldFR5cGUuUGxheWVyQnVsbGV0LCAxODAgKiBpIC8gdGhpcy5idWxsZXROdW1lcik7XHJcbiAgICAgICAgICAgICAgICBpKys7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5kZWxheSgwLjEpXHJcbiAgICAgICAgICAgIC51bmlvbigpXHJcbiAgICAgICAgICAgIC5yZXBlYXQodGhpcy5idWxsZXROdW1lcilcclxuICAgICAgICAgICAgLnN0YXJ0KClcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/BulletManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd8591kAOjNGzYo25bUiwq2G', 'BulletManager');
// Scripts/BulletManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BulletType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BulletType;
(function (BulletType) {
    BulletType[BulletType["PlayerBullet"] = 0] = "PlayerBullet";
    BulletType[BulletType["EnemyBullet"] = 1] = "EnemyBullet";
    BulletType[BulletType["SpecialBullet"] = 2] = "SpecialBullet";
    // 可以添加更多类型
})(BulletType = exports.BulletType || (exports.BulletType = {}));
var BulletManager = /** @class */ (function (_super) {
    __extends(BulletManager, _super);
    function BulletManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bulletPrefabs = [];
        _this.mainCamera = null;
        _this.bullets = [];
        _this.bulletPools = {};
        return _this;
    }
    BulletManager.prototype.onLoad = function () {
        // 为每种子弹类型创建对象池
        for (var i = 0; i < this.bulletPrefabs.length; i++) {
            this.bulletPools[i] = new cc.NodePool();
        }
        this.mainCamera = cc.find("Canvas/Main Camera");
    };
    BulletManager.prototype.createBullet = function (position, direction, speed, bulletType, angle) {
        var bullet;
        var pool = this.bulletPools[bulletType];
        if (pool.size() > 0) {
            bullet = pool.get();
        }
        else {
            bullet = cc.instantiate(this.bulletPrefabs[bulletType]);
        }
        bullet.getComponent('Bullet').init(position, direction, speed, bulletType, angle);
        cc.find("Canvas").addChild(bullet);
        this.bullets.push(bullet);
    };
    BulletManager.prototype.update = function (dt) {
        for (var i = this.bullets.length - 1; i >= 0; i--) {
            var bullet = this.bullets[i];
            bullet.getComponent('Bullet').updateBullet(dt);
            if (this.isOutOfScreen(bullet)) {
                this.bullets.splice(i, 1);
                this.bulletPools[bullet.getComponent('Bullet').bulletType].put(bullet);
            }
        }
    };
    BulletManager.prototype.getPoolSize = function (bulletType) {
        return this.bulletPools[bulletType].size();
    };
    BulletManager.prototype.isOutOfScreen = function (bullet) {
        var screenPos = this.mainCamera.position;
        var screenSize = cc.winSize;
        return (screenPos.x < -screenSize.width / 2 ||
            screenPos.x > screenSize.width / 2 ||
            screenPos.y < -screenSize.height / 2 ||
            screenPos.y > screenSize.height / 2);
    };
    __decorate([
        property([cc.Prefab])
    ], BulletManager.prototype, "bulletPrefabs", void 0);
    BulletManager = __decorate([
        ccclass
    ], BulletManager);
    return BulletManager;
}(cc.Component));
exports.default = BulletManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnVsbGV0TWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFFNUMsSUFBWSxVQUtYO0FBTEQsV0FBWSxVQUFVO0lBQ2xCLDJEQUFZLENBQUE7SUFDWix5REFBVyxDQUFBO0lBQ1gsNkRBQWEsQ0FBQTtJQUNiLFdBQVc7QUFDZixDQUFDLEVBTFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFLckI7QUFFRDtJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQXdEQztRQXRERyxtQkFBYSxHQUFnQixFQUFFLENBQUM7UUFFeEIsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFDM0IsYUFBTyxHQUFjLEVBQUUsQ0FBQztRQUN4QixpQkFBVyxHQUFtQyxFQUFFLENBQUM7O0lBa0Q3RCxDQUFDO0lBaERHLDhCQUFNLEdBQU47UUFDSSxlQUFlO1FBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsb0NBQVksR0FBWixVQUFhLFFBQWlCLEVBQUUsU0FBa0IsRUFBRSxLQUFhLEVBQUUsVUFBc0IsRUFBRSxLQUFhO1FBQ3BHLElBQUksTUFBZSxDQUFDO1FBQ3BCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFeEMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFO1lBQ2pCLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDdkI7YUFBTTtZQUNILE1BQU0sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztTQUMzRDtRQUNELE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUVsRixFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsOEJBQU0sR0FBTixVQUFPLEVBQVU7UUFDYixLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFL0MsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDMUU7U0FDSjtJQUNMLENBQUM7SUFDRCxtQ0FBVyxHQUFYLFVBQVksVUFBc0I7UUFDOUIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFDRCxxQ0FBYSxHQUFiLFVBQWMsTUFBZTtRQUN6QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQztRQUN6QyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDO1FBRTVCLE9BQU8sQ0FDSCxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDO1lBQ25DLFNBQVMsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDO1lBQ2xDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUM7WUFDcEMsU0FBUyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FDdEMsQ0FBQztJQUNOLENBQUM7SUFyREQ7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7d0RBQ1U7SUFGZixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBd0RqQztJQUFELG9CQUFDO0NBeERELEFBd0RDLENBeEQwQyxFQUFFLENBQUMsU0FBUyxHQXdEdEQ7a0JBeERvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmV4cG9ydCBlbnVtIEJ1bGxldFR5cGUge1xyXG4gICAgUGxheWVyQnVsbGV0LFxyXG4gICAgRW5lbXlCdWxsZXQsXHJcbiAgICBTcGVjaWFsQnVsbGV0LFxyXG4gICAgLy8g5Y+v5Lul5re75Yqg5pu05aSa57G75Z6LXHJcbn1cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnVsbGV0TWFuYWdlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBAcHJvcGVydHkoW2NjLlByZWZhYl0pXHJcbiAgICBidWxsZXRQcmVmYWJzOiBjYy5QcmVmYWJbXSA9IFtdO1xyXG5cclxuICAgIHByaXZhdGUgbWFpbkNhbWVyYTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBwcml2YXRlIGJ1bGxldHM6IGNjLk5vZGVbXSA9IFtdO1xyXG4gICAgcHJpdmF0ZSBidWxsZXRQb29sczogeyBba2V5OiBudW1iZXJdOiBjYy5Ob2RlUG9vbCB9ID0ge307XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIC8vIOS4uuavj+enjeWtkOW8ueexu+Wei+WIm+W7uuWvueixoeaxoFxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5idWxsZXRQcmVmYWJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYnVsbGV0UG9vbHNbaV0gPSBuZXcgY2MuTm9kZVBvb2woKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tYWluQ2FtZXJhID0gY2MuZmluZChcIkNhbnZhcy9NYWluIENhbWVyYVwiKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVCdWxsZXQocG9zaXRpb246IGNjLlZlYzIsIGRpcmVjdGlvbjogY2MuVmVjMiwgc3BlZWQ6IG51bWJlciwgYnVsbGV0VHlwZTogQnVsbGV0VHlwZSwgYW5nbGU6IG51bWJlcikge1xyXG4gICAgICAgIGxldCBidWxsZXQ6IGNjLk5vZGU7XHJcbiAgICAgICAgbGV0IHBvb2wgPSB0aGlzLmJ1bGxldFBvb2xzW2J1bGxldFR5cGVdO1xyXG5cclxuICAgICAgICBpZiAocG9vbC5zaXplKCkgPiAwKSB7XHJcbiAgICAgICAgICAgIGJ1bGxldCA9IHBvb2wuZ2V0KCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgYnVsbGV0ID0gY2MuaW5zdGFudGlhdGUodGhpcy5idWxsZXRQcmVmYWJzW2J1bGxldFR5cGVdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYnVsbGV0LmdldENvbXBvbmVudCgnQnVsbGV0JykuaW5pdChwb3NpdGlvbiwgZGlyZWN0aW9uLCBzcGVlZCwgYnVsbGV0VHlwZSwgYW5nbGUpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNjLmZpbmQoXCJDYW52YXNcIikuYWRkQ2hpbGQoYnVsbGV0KTtcclxuICAgICAgICB0aGlzLmJ1bGxldHMucHVzaChidWxsZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdDogbnVtYmVyKSB7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuYnVsbGV0cy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xyXG4gICAgICAgICAgICBsZXQgYnVsbGV0ID0gdGhpcy5idWxsZXRzW2ldO1xyXG4gICAgICAgICAgICBidWxsZXQuZ2V0Q29tcG9uZW50KCdCdWxsZXQnKS51cGRhdGVCdWxsZXQoZHQpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuaXNPdXRPZlNjcmVlbihidWxsZXQpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJ1bGxldHMuc3BsaWNlKGksIDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5idWxsZXRQb29sc1tidWxsZXQuZ2V0Q29tcG9uZW50KCdCdWxsZXQnKS5idWxsZXRUeXBlXS5wdXQoYnVsbGV0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGdldFBvb2xTaXplKGJ1bGxldFR5cGU6IEJ1bGxldFR5cGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5idWxsZXRQb29sc1tidWxsZXRUeXBlXS5zaXplKCk7XHJcbiAgICB9XHJcbiAgICBpc091dE9mU2NyZWVuKGJ1bGxldDogY2MuTm9kZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBzY3JlZW5Qb3MgPSB0aGlzLm1haW5DYW1lcmEucG9zaXRpb247XHJcbiAgICAgICAgbGV0IHNjcmVlblNpemUgPSBjYy53aW5TaXplO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICBzY3JlZW5Qb3MueCA8IC1zY3JlZW5TaXplLndpZHRoIC8gMiB8fFxyXG4gICAgICAgICAgICBzY3JlZW5Qb3MueCA+IHNjcmVlblNpemUud2lkdGggLyAyIHx8XHJcbiAgICAgICAgICAgIHNjcmVlblBvcy55IDwgLXNjcmVlblNpemUuaGVpZ2h0IC8gMiB8fFxyXG4gICAgICAgICAgICBzY3JlZW5Qb3MueSA+IHNjcmVlblNpemUuaGVpZ2h0IC8gMlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Bullet.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fc617yYs3RIr4uRtS95BXi0', 'Bullet');
// Scripts/Bullet.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BulletManager_1 = require("./BulletManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Bullet = /** @class */ (function (_super) {
    __extends(Bullet, _super);
    function Bullet() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.speed = 100;
        _this.direction = cc.Vec2.ZERO;
        _this.bulletType = BulletManager_1.BulletType.PlayerBullet;
        return _this;
    }
    Bullet.prototype.init = function (position, direction, speed, bulletType, angle) {
        this.node.x = position.x;
        this.node.y = position.y;
        this.direction = direction;
        this.speed = speed;
        this.bulletType = bulletType;
        this.node.angle = angle;
    };
    Bullet.prototype.updateBullet = function (dt) {
        console.log(this.node.x, this.node.y);
        var moveStep = this.direction.mul(this.speed * dt);
        this.node.x += moveStep.x;
        this.node.y += moveStep.y;
    };
    Bullet.prototype.onBeginContact = function (contact, self, other) {
        contact.disabled = true;
    };
    __decorate([
        property
    ], Bullet.prototype, "speed", void 0);
    Bullet = __decorate([
        ccclass
    ], Bullet);
    return Bullet;
}(cc.Component));
exports.default = Bullet;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnVsbGV0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlEQUEyQztBQUVyQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFvQywwQkFBWTtJQUFoRDtRQUFBLHFFQTJCQztRQXpCRyxXQUFLLEdBQVcsR0FBRyxDQUFDO1FBRXBCLGVBQVMsR0FBWSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNsQyxnQkFBVSxHQUFlLDBCQUFVLENBQUMsWUFBWSxDQUFDOztJQXNCckQsQ0FBQztJQXBCRyxxQkFBSSxHQUFKLFVBQUssUUFBaUIsRUFBRSxTQUFrQixFQUFFLEtBQWEsRUFBRSxVQUFzQixFQUFFLEtBQWE7UUFDNUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUU1QixDQUFDO0lBRUQsNkJBQVksR0FBWixVQUFhLEVBQVU7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBRTlCLENBQUM7SUFDRCwrQkFBYyxHQUFkLFVBQWUsT0FBTyxFQUFFLElBQUksRUFBRSxLQUFLO1FBQy9CLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUF4QkQ7UUFEQyxRQUFRO3lDQUNXO0lBRkgsTUFBTTtRQUQxQixPQUFPO09BQ2EsTUFBTSxDQTJCMUI7SUFBRCxhQUFDO0NBM0JELEFBMkJDLENBM0JtQyxFQUFFLENBQUMsU0FBUyxHQTJCL0M7a0JBM0JvQixNQUFNIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtCdWxsZXRUeXBlfSBmcm9tIFwiLi9CdWxsZXRNYW5hZ2VyXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnVsbGV0IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgc3BlZWQ6IG51bWJlciA9IDEwMDtcclxuXHJcbiAgICBkaXJlY3Rpb246IGNjLlZlYzIgPSBjYy5WZWMyLlpFUk87XHJcbiAgICBidWxsZXRUeXBlOiBCdWxsZXRUeXBlID0gQnVsbGV0VHlwZS5QbGF5ZXJCdWxsZXQ7XHJcblxyXG4gICAgaW5pdChwb3NpdGlvbjogY2MuVmVjMiwgZGlyZWN0aW9uOiBjYy5WZWMyLCBzcGVlZDogbnVtYmVyLCBidWxsZXRUeXBlOiBCdWxsZXRUeXBlLCBhbmdsZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5ub2RlLnggPSBwb3NpdGlvbi54O1xyXG4gICAgICAgIHRoaXMubm9kZS55ID0gcG9zaXRpb24ueTtcclxuICAgICAgICB0aGlzLmRpcmVjdGlvbiA9IGRpcmVjdGlvbjtcclxuICAgICAgICB0aGlzLnNwZWVkID0gc3BlZWQ7XHJcbiAgICAgICAgdGhpcy5idWxsZXRUeXBlID0gYnVsbGV0VHlwZTtcclxuICAgICAgICB0aGlzLm5vZGUuYW5nbGUgPSBhbmdsZTtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVCdWxsZXQoZHQ6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSk7XHJcbiAgICAgICAgbGV0IG1vdmVTdGVwID0gdGhpcy5kaXJlY3Rpb24ubXVsKHRoaXMuc3BlZWQgKiBkdCk7XHJcbiAgICAgICAgdGhpcy5ub2RlLnggKz0gbW92ZVN0ZXAueDtcclxuICAgICAgICB0aGlzLm5vZGUueSArPSBtb3ZlU3RlcC55O1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgb25CZWdpbkNvbnRhY3QoY29udGFjdCwgc2VsZiwgb3RoZXIpIHtcclxuICAgICAgICBjb250YWN0LmRpc2FibGVkID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Duck.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '24b2426FgtLTr1Kqx6MbQqS', 'Duck');
// Scripts/Duck.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Duck = /** @class */ (function (_super) {
    __extends(Duck, _super);
    function Duck() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.leftDown = false;
        _this.rightDown = false;
        _this.upDown = false;
        _this.downDown = false;
        _this.mainCamera = null;
        _this.jumping = false;
        _this.xSpeed = 0;
        _this.xMoveSpeed = 0;
        _this.yMoveSpeed = 0;
        _this.floatUpSpeed = 0;
        _this.DiveSpeed = 0;
        _this.levelNow = 0;
        _this.life = 3;
        _this.jumpDelay = 0;
        return _this;
    }
    Duck.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    };
    Duck.prototype.start = function () {
        this.mainCamera = cc.find("Canvas/Main Camera");
        this.xSpeed = this.xMoveSpeed;
    };
    Duck.prototype.update = function (dt) {
        this.moveControl(dt);
    };
    Duck.prototype.moveControl = function (dt) {
        var _this = this;
        // 左右
        if (this.leftDown) {
            this.xSpeed = -this.xMoveSpeed;
            if (this.node.scaleX > 0)
                this.node.scaleX *= -1;
        }
        else if (this.rightDown) {
            this.xSpeed = this.xMoveSpeed;
            if (this.node.scaleX < 0)
                this.node.scaleX *= -1;
        }
        // 跳躍
        if (this.upDown) {
            if (this.levelNow == 1) { //陸地
                if (this.jumping == false) {
                    this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 500);
                    this.jumping = true;
                    this.scheduleOnce(function () { _this.jumping = false; }, this.jumpDelay);
                }
            }
            else if (this.levelNow == 2) { //海洋
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -this.DiveSpeed);
            }
            else if (this.levelNow == 3) { //太空
                console.log(this.yMoveSpeed);
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.yMoveSpeed);
            }
        }
        else {
            if (this.levelNow == 2) { //海洋
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.floatUpSpeed);
            }
        }
        //往下
        if (this.downDown) {
            if (this.levelNow == 3) { //太空
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -this.yMoveSpeed);
            }
        }
        // 更新
        this.node.x += this.xSpeed * dt;
        this.cameraControl();
    };
    Duck.prototype.cameraControl = function () {
        this.mainCamera.x = this.node.x - 100;
    };
    Duck.prototype.getHurt = function () {
        this.life--;
    };
    Duck.prototype.onKeyDown = function (event) {
        // set a flag when key pressed
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = true;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
                this.upDown = true;
                break;
            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
                this.downDown = true;
                break;
        }
    };
    Duck.prototype.onKeyUp = function (event) {
        // unset a flag when key released
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = false;
                if (this.levelNow != 3)
                    this.xSpeed = 0;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = false;
                if (this.levelNow != 3)
                    this.xSpeed = 0;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
                this.upDown = false;
                break;
            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
                this.downDown = false;
                break;
        }
    };
    __decorate([
        property
    ], Duck.prototype, "xMoveSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "yMoveSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "floatUpSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "DiveSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "levelNow", void 0);
    __decorate([
        property
    ], Duck.prototype, "life", void 0);
    __decorate([
        property
    ], Duck.prototype, "jumpDelay", void 0);
    Duck = __decorate([
        ccclass
    ], Duck);
    return Duck;
}(cc.Component));
exports.default = Duck;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcRHVjay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFrQyx3QkFBWTtJQUE5QztRQUFBLHFFQWlKQztRQS9JVyxjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTSxHQUFZLEtBQUssQ0FBQztRQUN4QixjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGdCQUFVLEdBQVksSUFBSSxDQUFDO1FBQzNCLGFBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsWUFBTSxHQUFXLENBQUMsQ0FBQztRQUczQixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUV2QixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUV2QixrQkFBWSxHQUFXLENBQUMsQ0FBQztRQUV6QixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBRXRCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFFckIsVUFBSSxHQUFXLENBQUMsQ0FBQztRQUVqQixlQUFTLEdBQVcsQ0FBQyxDQUFDOztJQTBIMUIsQ0FBQztJQXhIRyxxQkFBTSxHQUFOO1FBQ0ksRUFBRSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDL0MsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0UsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFFM0UsQ0FBQztJQUVELG9CQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDbEMsQ0FBQztJQUVELHFCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUV6QixDQUFDO0lBQ0QsMEJBQVcsR0FBWCxVQUFhLEVBQUU7UUFBZixpQkErQ0M7UUE5Q0csS0FBSztRQUNMLElBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNkLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQy9CLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQztnQkFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDOUI7YUFDSSxJQUFHLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQzlCLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQztnQkFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUM7U0FFOUI7UUFFRCxLQUFLO1FBQ0wsSUFBRyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1osSUFBRyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsRUFBRSxFQUFLLElBQUk7Z0JBQzVCLElBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDL0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBSyxLQUFJLENBQUMsT0FBTyxHQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ2hFO2FBQ0o7aUJBQ0ksSUFBRyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsRUFBRSxFQUFJLElBQUk7Z0JBQ2hDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM5RTtpQkFDSSxJQUFHLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxFQUFFLEVBQUksSUFBSTtnQkFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDOUU7U0FDSjthQUNJO1lBQ0QsSUFBRyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsRUFBRSxFQUFJLElBQUk7Z0JBQzNCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDaEY7U0FDSjtRQUVELElBQUk7UUFDSixJQUFHLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZCxJQUFHLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSTtnQkFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQy9FO1NBQ0o7UUFFRCxLQUFLO1FBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCw0QkFBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQzFDLENBQUM7SUFDRCxzQkFBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFDRCx3QkFBUyxHQUFULFVBQVcsS0FBSztRQUNaLDhCQUE4QjtRQUM5QixRQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDbEIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJO2dCQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDckIsTUFBTTtZQUNWLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSztnQkFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3RCLE1BQU07WUFDVixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUN4QixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztZQUNyQixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ25CLE1BQU07WUFDVixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwQixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUk7Z0JBQ2xCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixNQUFNO1NBQ2I7SUFDTCxDQUFDO0lBQ0Qsc0JBQU8sR0FBUCxVQUFTLEtBQUs7UUFDVixpQ0FBaUM7UUFDakMsUUFBTyxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2xCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSTtnQkFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDO29CQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDcEIsTUFBTTtZQUNWLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSztnQkFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDO29CQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDcEIsTUFBTTtZQUNWLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3hCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO1lBQ3JCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDcEIsTUFBTTtZQUNWLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSTtnQkFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE1BQU07U0FDYjtJQUVMLENBQUM7SUFySUQ7UUFEQyxRQUFROzRDQUNjO0lBRXZCO1FBREMsUUFBUTs0Q0FDYztJQUV2QjtRQURDLFFBQVE7OENBQ2dCO0lBRXpCO1FBREMsUUFBUTsyQ0FDYTtJQUV0QjtRQURDLFFBQVE7MENBQ1k7SUFFckI7UUFEQyxRQUFRO3NDQUNRO0lBRWpCO1FBREMsUUFBUTsyQ0FDYTtJQXZCTCxJQUFJO1FBRHhCLE9BQU87T0FDYSxJQUFJLENBaUp4QjtJQUFELFdBQUM7Q0FqSkQsQUFpSkMsQ0FqSmlDLEVBQUUsQ0FBQyxTQUFTLEdBaUo3QztrQkFqSm9CLElBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIER1Y2sgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIHByaXZhdGUgbGVmdERvd246IGJvb2xlYW4gPSBmYWxzZTsgXHJcbiAgICBwcml2YXRlIHJpZ2h0RG93bjogYm9vbGVhbiA9IGZhbHNlOyBcclxuICAgIHByaXZhdGUgdXBEb3duOiBib29sZWFuID0gZmFsc2U7IFxyXG4gICAgcHJpdmF0ZSBkb3duRG93bjogYm9vbGVhbiA9IGZhbHNlOyBcclxuICAgIHByaXZhdGUgbWFpbkNhbWVyYTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBwcml2YXRlIGp1bXBpbmc6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHByaXZhdGUgeFNwZWVkOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgeE1vdmVTcGVlZDogbnVtYmVyID0gMDtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgeU1vdmVTcGVlZDogbnVtYmVyID0gMDtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgZmxvYXRVcFNwZWVkOiBudW1iZXIgPSAwO1xyXG4gICAgQHByb3BlcnR5XHJcbiAgICBEaXZlU3BlZWQ6IG51bWJlciA9IDA7XHJcbiAgICBAcHJvcGVydHlcclxuICAgIGxldmVsTm93OiBudW1iZXIgPSAwO1xyXG4gICAgQHByb3BlcnR5XHJcbiAgICBsaWZlOiBudW1iZXIgPSAzO1xyXG4gICAgQHByb3BlcnR5XHJcbiAgICBqdW1wRGVsYXk6IG51bWJlciA9IDA7XHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICBjYy5kaXJlY3Rvci5nZXRQaHlzaWNzTWFuYWdlcigpLmVuYWJsZWTCoD3CoHRydWU7XHJcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub24oY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9ET1dOLCB0aGlzLm9uS2V5RG93biwgdGhpcyk7XHJcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub24oY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9VUCwgdGhpcy5vbktleVVwLCB0aGlzKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQgKCkge1xyXG4gICAgICAgIHRoaXMubWFpbkNhbWVyYSA9IGNjLmZpbmQoXCJDYW52YXMvTWFpbiBDYW1lcmFcIik7XHJcbiAgICAgICAgdGhpcy54U3BlZWQgPSB0aGlzLnhNb3ZlU3BlZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKGR0KSB7XHJcbiAgICAgICAgdGhpcy5tb3ZlQ29udHJvbChkdCk7XHJcblxyXG4gICAgfVxyXG4gICAgbW92ZUNvbnRyb2wgKGR0KSB7XHJcbiAgICAgICAgLy8g5bem5Y+zXHJcbiAgICAgICAgaWYodGhpcy5sZWZ0RG93bikge1xyXG4gICAgICAgICAgICB0aGlzLnhTcGVlZCA9IC10aGlzLnhNb3ZlU3BlZWQ7XHJcbiAgICAgICAgICAgIGlmKHRoaXMubm9kZS5zY2FsZVggPiAwKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnNjYWxlWCAqPSAtMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZih0aGlzLnJpZ2h0RG93bikge1xyXG4gICAgICAgICAgICB0aGlzLnhTcGVlZCA9IHRoaXMueE1vdmVTcGVlZDtcclxuICAgICAgICAgICAgaWYodGhpcy5ub2RlLnNjYWxlWCA8IDApXHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuc2NhbGVYICo9IC0xO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8g6Lez6LqNXHJcbiAgICAgICAgaWYodGhpcy51cERvd24pIHtcclxuICAgICAgICAgICAgaWYodGhpcy5sZXZlbE5vdyA9PSAxKSB7ICAgIC8v6Zm45ZywXHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLmp1bXBpbmcgPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIoMCwgNTAwKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmp1bXBpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpPT57dGhpcy5qdW1waW5nPWZhbHNlO30sIHRoaXMuanVtcERlbGF5KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmKHRoaXMubGV2ZWxOb3cgPT0gMikgeyAgIC8v5rW35rSLXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIoMCwgLXRoaXMuRGl2ZVNwZWVkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmKHRoaXMubGV2ZWxOb3cgPT0gMykgeyAgIC8v5aSq56m6XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnlNb3ZlU3BlZWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRDb21wb25lbnQoY2MuUmlnaWRCb2R5KS5saW5lYXJWZWxvY2l0eSA9IGNjLnYyKDAsIHRoaXMueU1vdmVTcGVlZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIGlmKHRoaXMubGV2ZWxOb3cgPT0gMikgeyAgIC8v5rW35rSLXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIoMCwgdGhpcy5mbG9hdFVwU3BlZWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL+W+gOS4i1xyXG4gICAgICAgIGlmKHRoaXMuZG93bkRvd24pIHtcclxuICAgICAgICAgICAgaWYodGhpcy5sZXZlbE5vdyA9PSAzKSB7IC8v5aSq56m6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmxpbmVhclZlbG9jaXR5ID0gY2MudjIoMCwgLXRoaXMueU1vdmVTcGVlZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIOabtOaWsFxyXG4gICAgICAgIHRoaXMubm9kZS54ICs9IHRoaXMueFNwZWVkICogZHQ7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFDb250cm9sKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FtZXJhQ29udHJvbCgpIHtcclxuICAgICAgICB0aGlzLm1haW5DYW1lcmEueCA9IHRoaXMubm9kZS54IC0gMTAwO1xyXG4gICAgfVxyXG4gICAgZ2V0SHVydCgpIHtcclxuICAgICAgICB0aGlzLmxpZmUtLTtcclxuICAgIH1cclxuICAgIG9uS2V5RG93biAoZXZlbnQpIHtcclxuICAgICAgICAvLyBzZXQgYSBmbGFnIHdoZW4ga2V5IHByZXNzZWRcclxuICAgICAgICBzd2l0Y2goZXZlbnQua2V5Q29kZSkge1xyXG4gICAgICAgICAgICBjYXNlIGNjLm1hY3JvLktFWS5hOlxyXG4gICAgICAgICAgICBjYXNlIGNjLm1hY3JvLktFWS5sZWZ0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5sZWZ0RG93biA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkuZDpcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkucmlnaHQ6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJpZ2h0RG93biA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkuc3BhY2U6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLnVwOlxyXG4gICAgICAgICAgICBjYXNlIGNjLm1hY3JvLktFWS53OlxyXG4gICAgICAgICAgICAgICAgdGhpcy51cERvd24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLnM6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLmRvd246XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvd25Eb3duID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIG9uS2V5VXAgKGV2ZW50KSB7XHJcbiAgICAgICAgLy8gdW5zZXQgYSBmbGFnIHdoZW4ga2V5IHJlbGVhc2VkXHJcbiAgICAgICAgc3dpdGNoKGV2ZW50LmtleUNvZGUpIHtcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkuYTpcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkubGVmdDpcclxuICAgICAgICAgICAgICAgIHRoaXMubGVmdERvd24gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMubGV2ZWxOb3cgIT0gMylcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnhTcGVlZCA9IDA7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkuZDpcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkucmlnaHQ6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJpZ2h0RG93biA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5sZXZlbE5vdyAhPSAzKVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMueFNwZWVkID0gMDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIGNjLm1hY3JvLktFWS5zcGFjZTpcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkudXA6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLnc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwRG93biA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLnM6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLmRvd246XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvd25Eb3duID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------
