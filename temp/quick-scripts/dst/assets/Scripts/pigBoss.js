
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/pigBoss.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '055a2GqhJNFDobE1/BmcAkf', 'pigBoss');
// Scripts/pigBoss.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Duck_js_1 = require("./Duck.js");
var BulletManager_1 = require("./BulletManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var pigBoss = /** @class */ (function (_super) {
    __extends(pigBoss, _super);
    function pigBoss() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.originX = 0;
        _this.xSpeed = 100;
        _this.bulletManager = null;
        _this.duck = null;
        _this.bulletPrefab = null;
        _this.bulletFrame = null;
        _this.bulletNumer = 9;
        _this.bulletSpeed = 200;
        _this.moveRange = 50;
        return _this;
    }
    pigBoss.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
    };
    pigBoss.prototype.start = function () {
        this.originX = this.node.x;
    };
    pigBoss.prototype.update = function (dt) {
        if (Math.abs(this.node.x - this.originX) >= this.moveRange && Math.abs(this.node.x + this.xSpeed - this.originX) >= Math.abs(this.node.x - this.originX)) {
            this.xSpeed *= -1;
        }
        this.node.x += this.xSpeed * dt;
    };
    pigBoss.prototype.shoot = function () {
        var _this = this;
        var radius = 150;
        var i = 0;
        cc.tween(this.node)
            .call(function () {
            var a = _this.node.x + radius * Math.cos(Math.PI * i / _this.bulletNumer);
            var b = _this.node.y + radius * Math.sin(Math.PI * i / _this.bulletNumer);
            _this.bulletManager.createBullet(cc.v2(a, b), cc.v2(Math.cos(Math.PI * i / _this.bulletNumer), Math.sin(Math.PI * i / _this.bulletNumer)), _this.bulletSpeed, BulletManager_1.BulletType.PlayerBullet, 180 * i / _this.bulletNumer);
            i++;
        })
            .delay(0.1)
            .union()
            .repeat(this.bulletNumer)
            .start();
    };
    __decorate([
        property(BulletManager_1.default)
    ], pigBoss.prototype, "bulletManager", void 0);
    __decorate([
        property(Duck_js_1.default)
    ], pigBoss.prototype, "duck", void 0);
    __decorate([
        property(cc.Prefab)
    ], pigBoss.prototype, "bulletPrefab", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], pigBoss.prototype, "bulletFrame", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "bulletNumer", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "bulletSpeed", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "moveRange", void 0);
    pigBoss = __decorate([
        ccclass
    ], pigBoss);
    return pigBoss;
}(cc.Component));
exports.default = pigBoss;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xccGlnQm9zcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxxQ0FBNEI7QUFDNUIsaURBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXFDLDJCQUFZO0lBQWpEO1FBQUEscUVBdURDO1FBckRXLGFBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIsWUFBTSxHQUFXLEdBQUcsQ0FBQztRQUc3QixtQkFBYSxHQUFrQixJQUFJLENBQUM7UUFFcEMsVUFBSSxHQUFTLElBQUksQ0FBQztRQUVsQixrQkFBWSxHQUFjLElBQUksQ0FBQztRQUUvQixpQkFBVyxHQUFtQixJQUFJLENBQUM7UUFFbkMsaUJBQVcsR0FBVyxDQUFDLENBQUM7UUFFeEIsaUJBQVcsR0FBVyxHQUFHLENBQUM7UUFFMUIsZUFBUyxHQUFXLEVBQUUsQ0FBQzs7SUFxQzNCLENBQUM7SUFsQ0csd0JBQU0sR0FBTjtRQUNJLEVBQUUsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ25ELENBQUM7SUFFRCx1QkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUUvQixDQUFDO0lBRUQsd0JBQU0sR0FBTixVQUFRLEVBQUU7UUFDTixJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM3SSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3JCO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFHcEMsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFBQSxpQkFlQztRQWRHLElBQUksTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDVixFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDZCxJQUFJLENBQUM7WUFDRixJQUFJLENBQUMsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3hFLEtBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsRUFDekgsS0FBSSxDQUFDLFdBQVcsRUFBRSwwQkFBVSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuRixDQUFDLEVBQUUsQ0FBQztRQUNSLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxHQUFHLENBQUM7YUFDVixLQUFLLEVBQUU7YUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQzthQUN4QixLQUFLLEVBQUUsQ0FBQTtJQUNoQixDQUFDO0lBaEREO1FBREMsUUFBUSxDQUFDLHVCQUFhLENBQUM7a0RBQ1k7SUFFcEM7UUFEQyxRQUFRLENBQUMsaUJBQUksQ0FBQzt5Q0FDRztJQUVsQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2lEQUNXO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7Z0RBQ1U7SUFFbkM7UUFEQyxRQUFRO2dEQUNlO0lBRXhCO1FBREMsUUFBUTtnREFDaUI7SUFFMUI7UUFEQyxRQUFROzhDQUNjO0lBbEJOLE9BQU87UUFEM0IsT0FBTztPQUNhLE9BQU8sQ0F1RDNCO0lBQUQsY0FBQztDQXZERCxBQXVEQyxDQXZEb0MsRUFBRSxDQUFDLFNBQVMsR0F1RGhEO2tCQXZEb0IsT0FBTyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBEdWNrIGZyb20gXCIuL0R1Y2suanNcIlxyXG5pbXBvcnQgQnVsbGV0TWFuYWdlciwge0J1bGxldFR5cGV9IGZyb20gXCIuL0J1bGxldE1hbmFnZXJcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgcGlnQm9zcyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgcHJpdmF0ZSBvcmlnaW5YOiBudW1iZXIgPSAwO1xyXG4gICAgcHJpdmF0ZSB4U3BlZWQ6IG51bWJlciA9IDEwMDtcclxuXHJcbiAgICBAcHJvcGVydHkoQnVsbGV0TWFuYWdlcilcclxuICAgIGJ1bGxldE1hbmFnZXI6IEJ1bGxldE1hbmFnZXIgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KER1Y2spXHJcbiAgICBkdWNrOiBEdWNrID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5QcmVmYWIpXHJcbiAgICBidWxsZXRQcmVmYWI6IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXHJcbiAgICBidWxsZXRGcmFtZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5XHJcbiAgICBidWxsZXROdW1lcjogbnVtYmVyID0gOTtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgYnVsbGV0U3BlZWQ6IG51bWJlciA9IDIwMDtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgbW92ZVJhbmdlOiBudW1iZXIgPSA1MDtcclxuXHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICBjYy5kaXJlY3Rvci5nZXRQaHlzaWNzTWFuYWdlcigpLmVuYWJsZWTCoD3CoHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQgKCkge1xyXG4gICAgICAgIHRoaXMub3JpZ2luWCA9IHRoaXMubm9kZS54O1xyXG4gICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmKE1hdGguYWJzKHRoaXMubm9kZS54LXRoaXMub3JpZ2luWCkgPj0gdGhpcy5tb3ZlUmFuZ2UgJiYgTWF0aC5hYnModGhpcy5ub2RlLngrdGhpcy54U3BlZWQtdGhpcy5vcmlnaW5YKSA+PSBNYXRoLmFicyh0aGlzLm5vZGUueC10aGlzLm9yaWdpblgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMueFNwZWVkICo9IC0xO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5vZGUueCArPSB0aGlzLnhTcGVlZCAqIGR0O1xyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIHNob290ICgpIHtcclxuICAgICAgICBsZXQgcmFkaXVzID0gMTUwO1xyXG4gICAgICAgIGxldCBpID0gMDtcclxuICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXHJcbiAgICAgICAgICAgIC5jYWxsKCgpPT57XHJcbiAgICAgICAgICAgICAgICBsZXQgYSA9IHRoaXMubm9kZS54ICsgcmFkaXVzICogTWF0aC5jb3MoTWF0aC5QSSAqIGkgLyB0aGlzLmJ1bGxldE51bWVyKTtcclxuICAgICAgICAgICAgICAgIGxldCBiID0gdGhpcy5ub2RlLnkgKyByYWRpdXMgKiBNYXRoLnNpbihNYXRoLlBJICogaSAvIHRoaXMuYnVsbGV0TnVtZXIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5idWxsZXRNYW5hZ2VyLmNyZWF0ZUJ1bGxldChjYy52MihhLGIpLCBjYy52MihNYXRoLmNvcyhNYXRoLlBJICogaSAvIHRoaXMuYnVsbGV0TnVtZXIpLCBNYXRoLnNpbihNYXRoLlBJICogaSAvIHRoaXMuYnVsbGV0TnVtZXIpKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYnVsbGV0U3BlZWQsIEJ1bGxldFR5cGUuUGxheWVyQnVsbGV0LCAxODAgKiBpIC8gdGhpcy5idWxsZXROdW1lcik7XHJcbiAgICAgICAgICAgICAgICBpKys7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5kZWxheSgwLjEpXHJcbiAgICAgICAgICAgIC51bmlvbigpXHJcbiAgICAgICAgICAgIC5yZXBlYXQodGhpcy5idWxsZXROdW1lcilcclxuICAgICAgICAgICAgLnN0YXJ0KClcclxuICAgIH1cclxufVxyXG4iXX0=