
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/BulletManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd8591kAOjNGzYo25bUiwq2G', 'BulletManager');
// Scripts/BulletManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BulletType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BulletType;
(function (BulletType) {
    BulletType[BulletType["PlayerBullet"] = 0] = "PlayerBullet";
    BulletType[BulletType["EnemyBullet"] = 1] = "EnemyBullet";
    BulletType[BulletType["SpecialBullet"] = 2] = "SpecialBullet";
    // 可以添加更多类型
})(BulletType = exports.BulletType || (exports.BulletType = {}));
var BulletManager = /** @class */ (function (_super) {
    __extends(BulletManager, _super);
    function BulletManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bulletPrefabs = [];
        _this.mainCamera = null;
        _this.bullets = [];
        _this.bulletPools = {};
        return _this;
    }
    BulletManager.prototype.onLoad = function () {
        // 为每种子弹类型创建对象池
        for (var i = 0; i < this.bulletPrefabs.length; i++) {
            this.bulletPools[i] = new cc.NodePool();
        }
        this.mainCamera = cc.find("Canvas/Main Camera");
    };
    BulletManager.prototype.createBullet = function (position, direction, speed, bulletType, angle) {
        var bullet;
        var pool = this.bulletPools[bulletType];
        if (pool.size() > 0) {
            bullet = pool.get();
        }
        else {
            bullet = cc.instantiate(this.bulletPrefabs[bulletType]);
        }
        bullet.getComponent('Bullet').init(position, direction, speed, bulletType, angle);
        cc.find("Canvas").addChild(bullet);
        this.bullets.push(bullet);
    };
    BulletManager.prototype.update = function (dt) {
        for (var i = this.bullets.length - 1; i >= 0; i--) {
            var bullet = this.bullets[i];
            bullet.getComponent('Bullet').updateBullet(dt);
            if (this.isOutOfScreen(bullet)) {
                this.bullets.splice(i, 1);
                this.bulletPools[bullet.getComponent('Bullet').bulletType].put(bullet);
            }
        }
    };
    BulletManager.prototype.getPoolSize = function (bulletType) {
        return this.bulletPools[bulletType].size();
    };
    BulletManager.prototype.isOutOfScreen = function (bullet) {
        var screenPos = this.mainCamera.position;
        var screenSize = cc.winSize;
        return (screenPos.x < -screenSize.width / 2 ||
            screenPos.x > screenSize.width / 2 ||
            screenPos.y < -screenSize.height / 2 ||
            screenPos.y > screenSize.height / 2);
    };
    __decorate([
        property([cc.Prefab])
    ], BulletManager.prototype, "bulletPrefabs", void 0);
    BulletManager = __decorate([
        ccclass
    ], BulletManager);
    return BulletManager;
}(cc.Component));
exports.default = BulletManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnVsbGV0TWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFFNUMsSUFBWSxVQUtYO0FBTEQsV0FBWSxVQUFVO0lBQ2xCLDJEQUFZLENBQUE7SUFDWix5REFBVyxDQUFBO0lBQ1gsNkRBQWEsQ0FBQTtJQUNiLFdBQVc7QUFDZixDQUFDLEVBTFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFLckI7QUFFRDtJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQXdEQztRQXRERyxtQkFBYSxHQUFnQixFQUFFLENBQUM7UUFFeEIsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFDM0IsYUFBTyxHQUFjLEVBQUUsQ0FBQztRQUN4QixpQkFBVyxHQUFtQyxFQUFFLENBQUM7O0lBa0Q3RCxDQUFDO0lBaERHLDhCQUFNLEdBQU47UUFDSSxlQUFlO1FBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsb0NBQVksR0FBWixVQUFhLFFBQWlCLEVBQUUsU0FBa0IsRUFBRSxLQUFhLEVBQUUsVUFBc0IsRUFBRSxLQUFhO1FBQ3BHLElBQUksTUFBZSxDQUFDO1FBQ3BCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFeEMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFO1lBQ2pCLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDdkI7YUFBTTtZQUNILE1BQU0sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztTQUMzRDtRQUNELE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUVsRixFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsOEJBQU0sR0FBTixVQUFPLEVBQVU7UUFDYixLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFL0MsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDMUU7U0FDSjtJQUNMLENBQUM7SUFDRCxtQ0FBVyxHQUFYLFVBQVksVUFBc0I7UUFDOUIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFDRCxxQ0FBYSxHQUFiLFVBQWMsTUFBZTtRQUN6QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQztRQUN6QyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDO1FBRTVCLE9BQU8sQ0FDSCxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDO1lBQ25DLFNBQVMsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDO1lBQ2xDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUM7WUFDcEMsU0FBUyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FDdEMsQ0FBQztJQUNOLENBQUM7SUFyREQ7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7d0RBQ1U7SUFGZixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBd0RqQztJQUFELG9CQUFDO0NBeERELEFBd0RDLENBeEQwQyxFQUFFLENBQUMsU0FBUyxHQXdEdEQ7a0JBeERvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmV4cG9ydCBlbnVtIEJ1bGxldFR5cGUge1xyXG4gICAgUGxheWVyQnVsbGV0LFxyXG4gICAgRW5lbXlCdWxsZXQsXHJcbiAgICBTcGVjaWFsQnVsbGV0LFxyXG4gICAgLy8g5Y+v5Lul5re75Yqg5pu05aSa57G75Z6LXHJcbn1cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnVsbGV0TWFuYWdlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBAcHJvcGVydHkoW2NjLlByZWZhYl0pXHJcbiAgICBidWxsZXRQcmVmYWJzOiBjYy5QcmVmYWJbXSA9IFtdO1xyXG5cclxuICAgIHByaXZhdGUgbWFpbkNhbWVyYTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBwcml2YXRlIGJ1bGxldHM6IGNjLk5vZGVbXSA9IFtdO1xyXG4gICAgcHJpdmF0ZSBidWxsZXRQb29sczogeyBba2V5OiBudW1iZXJdOiBjYy5Ob2RlUG9vbCB9ID0ge307XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIC8vIOS4uuavj+enjeWtkOW8ueexu+Wei+WIm+W7uuWvueixoeaxoFxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5idWxsZXRQcmVmYWJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYnVsbGV0UG9vbHNbaV0gPSBuZXcgY2MuTm9kZVBvb2woKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tYWluQ2FtZXJhID0gY2MuZmluZChcIkNhbnZhcy9NYWluIENhbWVyYVwiKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVCdWxsZXQocG9zaXRpb246IGNjLlZlYzIsIGRpcmVjdGlvbjogY2MuVmVjMiwgc3BlZWQ6IG51bWJlciwgYnVsbGV0VHlwZTogQnVsbGV0VHlwZSwgYW5nbGU6IG51bWJlcikge1xyXG4gICAgICAgIGxldCBidWxsZXQ6IGNjLk5vZGU7XHJcbiAgICAgICAgbGV0IHBvb2wgPSB0aGlzLmJ1bGxldFBvb2xzW2J1bGxldFR5cGVdO1xyXG5cclxuICAgICAgICBpZiAocG9vbC5zaXplKCkgPiAwKSB7XHJcbiAgICAgICAgICAgIGJ1bGxldCA9IHBvb2wuZ2V0KCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgYnVsbGV0ID0gY2MuaW5zdGFudGlhdGUodGhpcy5idWxsZXRQcmVmYWJzW2J1bGxldFR5cGVdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYnVsbGV0LmdldENvbXBvbmVudCgnQnVsbGV0JykuaW5pdChwb3NpdGlvbiwgZGlyZWN0aW9uLCBzcGVlZCwgYnVsbGV0VHlwZSwgYW5nbGUpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNjLmZpbmQoXCJDYW52YXNcIikuYWRkQ2hpbGQoYnVsbGV0KTtcclxuICAgICAgICB0aGlzLmJ1bGxldHMucHVzaChidWxsZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdDogbnVtYmVyKSB7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IHRoaXMuYnVsbGV0cy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xyXG4gICAgICAgICAgICBsZXQgYnVsbGV0ID0gdGhpcy5idWxsZXRzW2ldO1xyXG4gICAgICAgICAgICBidWxsZXQuZ2V0Q29tcG9uZW50KCdCdWxsZXQnKS51cGRhdGVCdWxsZXQoZHQpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuaXNPdXRPZlNjcmVlbihidWxsZXQpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJ1bGxldHMuc3BsaWNlKGksIDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5idWxsZXRQb29sc1tidWxsZXQuZ2V0Q29tcG9uZW50KCdCdWxsZXQnKS5idWxsZXRUeXBlXS5wdXQoYnVsbGV0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGdldFBvb2xTaXplKGJ1bGxldFR5cGU6IEJ1bGxldFR5cGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5idWxsZXRQb29sc1tidWxsZXRUeXBlXS5zaXplKCk7XHJcbiAgICB9XHJcbiAgICBpc091dE9mU2NyZWVuKGJ1bGxldDogY2MuTm9kZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBzY3JlZW5Qb3MgPSB0aGlzLm1haW5DYW1lcmEucG9zaXRpb247XHJcbiAgICAgICAgbGV0IHNjcmVlblNpemUgPSBjYy53aW5TaXplO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICBzY3JlZW5Qb3MueCA8IC1zY3JlZW5TaXplLndpZHRoIC8gMiB8fFxyXG4gICAgICAgICAgICBzY3JlZW5Qb3MueCA+IHNjcmVlblNpemUud2lkdGggLyAyIHx8XHJcbiAgICAgICAgICAgIHNjcmVlblBvcy55IDwgLXNjcmVlblNpemUuaGVpZ2h0IC8gMiB8fFxyXG4gICAgICAgICAgICBzY3JlZW5Qb3MueSA+IHNjcmVlblNpemUuaGVpZ2h0IC8gMlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19