
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Bullet.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fc617yYs3RIr4uRtS95BXi0', 'Bullet');
// Scripts/Bullet.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BulletManager_1 = require("./BulletManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Bullet = /** @class */ (function (_super) {
    __extends(Bullet, _super);
    function Bullet() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.speed = 100;
        _this.direction = cc.Vec2.ZERO;
        _this.bulletType = BulletManager_1.BulletType.PlayerBullet;
        return _this;
    }
    Bullet.prototype.init = function (position, direction, speed, bulletType, angle) {
        this.node.x = position.x;
        this.node.y = position.y;
        this.direction = direction;
        this.speed = speed;
        this.bulletType = bulletType;
        this.node.angle = angle;
    };
    Bullet.prototype.updateBullet = function (dt) {
        console.log(this.node.x, this.node.y);
        var moveStep = this.direction.mul(this.speed * dt);
        this.node.x += moveStep.x;
        this.node.y += moveStep.y;
    };
    Bullet.prototype.onBeginContact = function (contact, self, other) {
        contact.disabled = true;
    };
    __decorate([
        property
    ], Bullet.prototype, "speed", void 0);
    Bullet = __decorate([
        ccclass
    ], Bullet);
    return Bullet;
}(cc.Component));
exports.default = Bullet;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnVsbGV0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlEQUEyQztBQUVyQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFvQywwQkFBWTtJQUFoRDtRQUFBLHFFQTJCQztRQXpCRyxXQUFLLEdBQVcsR0FBRyxDQUFDO1FBRXBCLGVBQVMsR0FBWSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNsQyxnQkFBVSxHQUFlLDBCQUFVLENBQUMsWUFBWSxDQUFDOztJQXNCckQsQ0FBQztJQXBCRyxxQkFBSSxHQUFKLFVBQUssUUFBaUIsRUFBRSxTQUFrQixFQUFFLEtBQWEsRUFBRSxVQUFzQixFQUFFLEtBQWE7UUFDNUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUU1QixDQUFDO0lBRUQsNkJBQVksR0FBWixVQUFhLEVBQVU7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBRTlCLENBQUM7SUFDRCwrQkFBYyxHQUFkLFVBQWUsT0FBTyxFQUFFLElBQUksRUFBRSxLQUFLO1FBQy9CLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUF4QkQ7UUFEQyxRQUFRO3lDQUNXO0lBRkgsTUFBTTtRQUQxQixPQUFPO09BQ2EsTUFBTSxDQTJCMUI7SUFBRCxhQUFDO0NBM0JELEFBMkJDLENBM0JtQyxFQUFFLENBQUMsU0FBUyxHQTJCL0M7a0JBM0JvQixNQUFNIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtCdWxsZXRUeXBlfSBmcm9tIFwiLi9CdWxsZXRNYW5hZ2VyXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnVsbGV0IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgc3BlZWQ6IG51bWJlciA9IDEwMDtcclxuXHJcbiAgICBkaXJlY3Rpb246IGNjLlZlYzIgPSBjYy5WZWMyLlpFUk87XHJcbiAgICBidWxsZXRUeXBlOiBCdWxsZXRUeXBlID0gQnVsbGV0VHlwZS5QbGF5ZXJCdWxsZXQ7XHJcblxyXG4gICAgaW5pdChwb3NpdGlvbjogY2MuVmVjMiwgZGlyZWN0aW9uOiBjYy5WZWMyLCBzcGVlZDogbnVtYmVyLCBidWxsZXRUeXBlOiBCdWxsZXRUeXBlLCBhbmdsZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5ub2RlLnggPSBwb3NpdGlvbi54O1xyXG4gICAgICAgIHRoaXMubm9kZS55ID0gcG9zaXRpb24ueTtcclxuICAgICAgICB0aGlzLmRpcmVjdGlvbiA9IGRpcmVjdGlvbjtcclxuICAgICAgICB0aGlzLnNwZWVkID0gc3BlZWQ7XHJcbiAgICAgICAgdGhpcy5idWxsZXRUeXBlID0gYnVsbGV0VHlwZTtcclxuICAgICAgICB0aGlzLm5vZGUuYW5nbGUgPSBhbmdsZTtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVCdWxsZXQoZHQ6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSk7XHJcbiAgICAgICAgbGV0IG1vdmVTdGVwID0gdGhpcy5kaXJlY3Rpb24ubXVsKHRoaXMuc3BlZWQgKiBkdCk7XHJcbiAgICAgICAgdGhpcy5ub2RlLnggKz0gbW92ZVN0ZXAueDtcclxuICAgICAgICB0aGlzLm5vZGUueSArPSBtb3ZlU3RlcC55O1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgb25CZWdpbkNvbnRhY3QoY29udGFjdCwgc2VsZiwgb3RoZXIpIHtcclxuICAgICAgICBjb250YWN0LmRpc2FibGVkID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=