"use strict";
cc._RF.push(module, '24b2426FgtLTr1Kqx6MbQqS', 'Duck');
// Scripts/Duck.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Duck = /** @class */ (function (_super) {
    __extends(Duck, _super);
    function Duck() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.leftDown = false;
        _this.rightDown = false;
        _this.upDown = false;
        _this.downDown = false;
        _this.mainCamera = null;
        _this.jumping = false;
        _this.xSpeed = 0;
        _this.xMoveSpeed = 0;
        _this.yMoveSpeed = 0;
        _this.floatUpSpeed = 0;
        _this.DiveSpeed = 0;
        _this.levelNow = 0;
        _this.life = 3;
        _this.jumpDelay = 0;
        return _this;
    }
    Duck.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    };
    Duck.prototype.start = function () {
        this.mainCamera = cc.find("Canvas/Main Camera");
        this.xSpeed = this.xMoveSpeed;
    };
    Duck.prototype.update = function (dt) {
        this.moveControl(dt);
    };
    Duck.prototype.moveControl = function (dt) {
        var _this = this;
        // 左右
        if (this.leftDown) {
            this.xSpeed = -this.xMoveSpeed;
            if (this.node.scaleX > 0)
                this.node.scaleX *= -1;
        }
        else if (this.rightDown) {
            this.xSpeed = this.xMoveSpeed;
            if (this.node.scaleX < 0)
                this.node.scaleX *= -1;
        }
        // 跳躍
        if (this.upDown) {
            if (this.levelNow == 1) { //陸地
                if (this.jumping == false) {
                    this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 500);
                    this.jumping = true;
                    this.scheduleOnce(function () { _this.jumping = false; }, this.jumpDelay);
                }
            }
            else if (this.levelNow == 2) { //海洋
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -this.DiveSpeed);
            }
            else if (this.levelNow == 3) { //太空
                console.log(this.yMoveSpeed);
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.yMoveSpeed);
            }
        }
        else {
            if (this.levelNow == 2) { //海洋
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.floatUpSpeed);
            }
        }
        //往下
        if (this.downDown) {
            if (this.levelNow == 3) { //太空
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -this.yMoveSpeed);
            }
        }
        // 更新
        this.node.x += this.xSpeed * dt;
        this.cameraControl();
    };
    Duck.prototype.cameraControl = function () {
        this.mainCamera.x = this.node.x - 100;
    };
    Duck.prototype.getHurt = function () {
        this.life--;
    };
    Duck.prototype.onKeyDown = function (event) {
        // set a flag when key pressed
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = true;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
                this.upDown = true;
                break;
            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
                this.downDown = true;
                break;
        }
    };
    Duck.prototype.onKeyUp = function (event) {
        // unset a flag when key released
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = false;
                if (this.levelNow != 3)
                    this.xSpeed = 0;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = false;
                if (this.levelNow != 3)
                    this.xSpeed = 0;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
                this.upDown = false;
                break;
            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
                this.downDown = false;
                break;
        }
    };
    __decorate([
        property
    ], Duck.prototype, "xMoveSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "yMoveSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "floatUpSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "DiveSpeed", void 0);
    __decorate([
        property
    ], Duck.prototype, "levelNow", void 0);
    __decorate([
        property
    ], Duck.prototype, "life", void 0);
    __decorate([
        property
    ], Duck.prototype, "jumpDelay", void 0);
    Duck = __decorate([
        ccclass
    ], Duck);
    return Duck;
}(cc.Component));
exports.default = Duck;

cc._RF.pop();