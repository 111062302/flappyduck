"use strict";
cc._RF.push(module, '055a2GqhJNFDobE1/BmcAkf', 'pigBoss');
// Scripts/pigBoss.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Duck_js_1 = require("./Duck.js");
var BulletManager_1 = require("./BulletManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var pigBoss = /** @class */ (function (_super) {
    __extends(pigBoss, _super);
    function pigBoss() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.originX = 0;
        _this.xSpeed = 100;
        _this.bulletManager = null;
        _this.duck = null;
        _this.bulletPrefab = null;
        _this.bulletFrame = null;
        _this.bulletNumer = 9;
        _this.bulletSpeed = 200;
        _this.moveRange = 50;
        return _this;
    }
    pigBoss.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
    };
    pigBoss.prototype.start = function () {
        this.originX = this.node.x;
    };
    pigBoss.prototype.update = function (dt) {
        if (Math.abs(this.node.x - this.originX) >= this.moveRange && Math.abs(this.node.x + this.xSpeed - this.originX) >= Math.abs(this.node.x - this.originX)) {
            this.xSpeed *= -1;
        }
        this.node.x += this.xSpeed * dt;
    };
    pigBoss.prototype.shoot = function () {
        var _this = this;
        var radius = 150;
        var i = 0;
        cc.tween(this.node)
            .call(function () {
            var a = _this.node.x + radius * Math.cos(Math.PI * i / _this.bulletNumer);
            var b = _this.node.y + radius * Math.sin(Math.PI * i / _this.bulletNumer);
            _this.bulletManager.createBullet(cc.v2(a, b), cc.v2(Math.cos(Math.PI * i / _this.bulletNumer), Math.sin(Math.PI * i / _this.bulletNumer)), _this.bulletSpeed, BulletManager_1.BulletType.PlayerBullet, 180 * i / _this.bulletNumer);
            i++;
        })
            .delay(0.1)
            .union()
            .repeat(this.bulletNumer)
            .start();
    };
    __decorate([
        property(BulletManager_1.default)
    ], pigBoss.prototype, "bulletManager", void 0);
    __decorate([
        property(Duck_js_1.default)
    ], pigBoss.prototype, "duck", void 0);
    __decorate([
        property(cc.Prefab)
    ], pigBoss.prototype, "bulletPrefab", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], pigBoss.prototype, "bulletFrame", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "bulletNumer", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "bulletSpeed", void 0);
    __decorate([
        property
    ], pigBoss.prototype, "moveRange", void 0);
    pigBoss = __decorate([
        ccclass
    ], pigBoss);
    return pigBoss;
}(cc.Component));
exports.default = pigBoss;

cc._RF.pop();