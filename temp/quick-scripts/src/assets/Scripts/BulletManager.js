"use strict";
cc._RF.push(module, 'd8591kAOjNGzYo25bUiwq2G', 'BulletManager');
// Scripts/BulletManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BulletType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BulletType;
(function (BulletType) {
    BulletType[BulletType["PlayerBullet"] = 0] = "PlayerBullet";
    BulletType[BulletType["EnemyBullet"] = 1] = "EnemyBullet";
    BulletType[BulletType["SpecialBullet"] = 2] = "SpecialBullet";
    // 可以添加更多类型
})(BulletType = exports.BulletType || (exports.BulletType = {}));
var BulletManager = /** @class */ (function (_super) {
    __extends(BulletManager, _super);
    function BulletManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bulletPrefabs = [];
        _this.mainCamera = null;
        _this.bullets = [];
        _this.bulletPools = {};
        return _this;
    }
    BulletManager.prototype.onLoad = function () {
        // 为每种子弹类型创建对象池
        for (var i = 0; i < this.bulletPrefabs.length; i++) {
            this.bulletPools[i] = new cc.NodePool();
        }
        this.mainCamera = cc.find("Canvas/Main Camera");
    };
    BulletManager.prototype.createBullet = function (position, direction, speed, bulletType, angle) {
        var bullet;
        var pool = this.bulletPools[bulletType];
        if (pool.size() > 0) {
            bullet = pool.get();
        }
        else {
            bullet = cc.instantiate(this.bulletPrefabs[bulletType]);
        }
        bullet.getComponent('Bullet').init(position, direction, speed, bulletType, angle);
        cc.find("Canvas").addChild(bullet);
        this.bullets.push(bullet);
    };
    BulletManager.prototype.update = function (dt) {
        for (var i = this.bullets.length - 1; i >= 0; i--) {
            var bullet = this.bullets[i];
            bullet.getComponent('Bullet').updateBullet(dt);
            if (this.isOutOfScreen(bullet)) {
                this.bullets.splice(i, 1);
                this.bulletPools[bullet.getComponent('Bullet').bulletType].put(bullet);
            }
        }
    };
    BulletManager.prototype.getPoolSize = function (bulletType) {
        return this.bulletPools[bulletType].size();
    };
    BulletManager.prototype.isOutOfScreen = function (bullet) {
        var screenPos = this.mainCamera.position;
        var screenSize = cc.winSize;
        return (screenPos.x < -screenSize.width / 2 ||
            screenPos.x > screenSize.width / 2 ||
            screenPos.y < -screenSize.height / 2 ||
            screenPos.y > screenSize.height / 2);
    };
    __decorate([
        property([cc.Prefab])
    ], BulletManager.prototype, "bulletPrefabs", void 0);
    BulletManager = __decorate([
        ccclass
    ], BulletManager);
    return BulletManager;
}(cc.Component));
exports.default = BulletManager;

cc._RF.pop();