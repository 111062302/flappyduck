import Duck from "./Duck.js"
import BulletManager, {BulletType} from "./BulletManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class pigBoss extends cc.Component {

    private originX: number = 0;
    private xSpeed: number = 100;

    @property(BulletManager)
    bulletManager: BulletManager = null;
    @property(Duck)
    duck: Duck = null;
    @property(cc.Prefab)
    bulletPrefab: cc.Prefab = null;
    @property(cc.SpriteFrame)
    bulletFrame: cc.SpriteFrame = null;
    @property
    bulletNumer: number = 9;
    @property
    bulletSpeed: number = 200;
    @property
    moveRange: number = 50;


    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
    }

    start () {
        this.originX = this.node.x;
       
    }

    update (dt) {
        if(Math.abs(this.node.x-this.originX) >= this.moveRange && Math.abs(this.node.x+this.xSpeed-this.originX) >= Math.abs(this.node.x-this.originX)) {
            this.xSpeed *= -1;
        }
        this.node.x += this.xSpeed * dt;
        
        
    }

    shoot () {
        let radius = 150;
        let i = 0;
        cc.tween(this.node)
            .call(()=>{
                let a = this.node.x + radius * Math.cos(Math.PI * i / this.bulletNumer);
                let b = this.node.y + radius * Math.sin(Math.PI * i / this.bulletNumer);
                this.bulletManager.createBullet(cc.v2(a,b), cc.v2(Math.cos(Math.PI * i / this.bulletNumer), Math.sin(Math.PI * i / this.bulletNumer)),
                            this.bulletSpeed, BulletType.pigBossBullet, 180 * i / this.bulletNumer);
                i++;
            })
            .delay(0.1)
            .union()
            .repeat(this.bulletNumer)
            .start()
    }
}
