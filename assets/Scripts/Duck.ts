import BulletManager, {BulletType} from "./BulletManager";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Duck extends cc.Component {

    private leftDown: boolean = false;
    private shootDown: boolean = false; 
    private rightDown: boolean = false; 
    private upDown: boolean = false; 
    private downDown: boolean = false; 
    private mainCamera: cc.Node = null;
    private jumping: boolean = false;
    private shooting: boolean = false;
    private xSpeed: number = 0;

    @property(BulletManager)
    bulletManager: BulletManager = null;
    @property
    xMoveSpeed: number = 0;
    @property
    yMoveSpeed: number = 0;
    @property
    floatUpSpeed: number = 0;
    @property
    DiveSpeed: number = 0;
    @property
    levelNow: number = 0;
    @property
    life: number = 3;
    @property
    jumpDelay: number = 0;

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

    }

    start () {
        this.mainCamera = cc.find("Canvas/Main Camera");
        this.xSpeed = this.xMoveSpeed;
    }

    update(dt) {
        this.moveControl(dt);

    }
    moveControl (dt) {
        // 左右
        if(this.leftDown) {
            this.xSpeed = -this.xMoveSpeed;
            if(this.node.scaleX > 0)
                this.node.scaleX *= -1;
        }
        else if(this.rightDown) {
            this.xSpeed = this.xMoveSpeed;
            if(this.node.scaleX < 0)
                this.node.scaleX *= -1;
            
        }
        
        // 跳躍
        if(this.upDown) {
            if(this.levelNow == 1) {    //陸地
                if(this.jumping == false) {
                    this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 500);
                    this.jumping = true;
                    this.scheduleOnce(()=>{this.jumping=false;}, this.jumpDelay);
                }
            }
            else if(this.levelNow == 2) {   //海洋
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -this.DiveSpeed);
            }
            else if(this.levelNow == 3) {   //太空
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.yMoveSpeed);
            }
        }
        else {
            if(this.levelNow == 2) {   //海洋
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.floatUpSpeed);
            }
        }

        //往下
        if(this.downDown) {
            if(this.levelNow == 3) { //太空
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -this.yMoveSpeed);
            }
        }

        if(this.shootDown) {
            if(this.shooting == false) {
                this.bulletManager.createBullet(cc.v2(this.node.x,this.node.y),cc.v2(this.xSpeed>0?1:-1, 0),200,BulletType.duckBullet,this.xSpeed>0?0:180);
                this.shooting = true;
                this.scheduleOnce(()=>{this.shooting=false;}, 0.5);
            }
        }
        // 更新
        this.node.x += this.xSpeed * dt;
        this.cameraControl();
    }

    cameraControl() {
        this.mainCamera.x = this.node.x - 100;
    }
    getHurt() {
        this.life--;
    }
    onKeyDown (event) {
        // set a flag when key pressed
        switch(event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = true;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
                this.upDown = true;
                break;
            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
                this.downDown = true;
                break;
            case cc.macro.KEY.enter:
                this.shootDown = true;
                break;
        }
    }
    onKeyUp (event) {
        // unset a flag when key released
        switch(event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.leftDown = false;
                if(this.levelNow != 3)
                    this.xSpeed = 0;
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.rightDown = false;
                if(this.levelNow != 3)
                    this.xSpeed = 0;
                break;
            case cc.macro.KEY.space:
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
                this.upDown = false;
                break;
            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
                this.downDown = false;
                break;
            case cc.macro.KEY.enter:
                this.shootDown = false;
                break;
        }

    }
}
