const { ccclass, property } = cc._decorator;

export enum BulletType {
    duckBullet,
    pigBossBullet,
    SpecialBullet,
    // 可以添加更多类型
}
@ccclass
export default class BulletManager extends cc.Component {
    @property([cc.Prefab])
    bulletPrefabs: cc.Prefab[] = [];

    private mainCamera: cc.Node = null;
    private bullets: cc.Node[] = [];
    private bulletPools: { [key: number]: cc.NodePool } = {};

    onLoad() {
        // 为每种子弹类型创建对象池
        for (let i = 0; i < this.bulletPrefabs.length; i++) {
            this.bulletPools[i] = new cc.NodePool();
        }
        this.mainCamera = cc.find("Canvas/Main Camera");
    }

    createBullet(position: cc.Vec2, direction: cc.Vec2, speed: number, bulletType: BulletType, angle: number) {
        let bullet: cc.Node;
        let pool = this.bulletPools[bulletType];

        if (pool.size() > 0) {
            bullet = pool.get();
        } else {
            bullet = cc.instantiate(this.bulletPrefabs[bulletType]);
        }
        bullet.getComponent('Bullet').init(position, direction, speed, bulletType, angle);
        
        cc.find("Canvas").addChild(bullet);
        this.bullets.push(bullet);
    }

    update(dt: number) {
        for (let i = this.bullets.length - 1; i >= 0; i--) {
            let bullet = this.bullets[i];
            bullet.getComponent('Bullet').updateBullet(dt);

            if (this.isOutOfScreen(bullet)) {
                this.bullets.splice(i, 1);
                this.bulletPools[bullet.getComponent('Bullet').bulletType].put(bullet);
            }
        }
    }
    getPoolSize(bulletType: BulletType) {
        return this.bulletPools[bulletType].size();
    }
    isOutOfScreen(bullet: cc.Node): boolean {
        let screenPos = this.mainCamera.position;
        let screenSize = cc.winSize;

        return (
            screenPos.x < -screenSize.width / 2 ||
            screenPos.x > screenSize.width / 2 ||
            screenPos.y < -screenSize.height / 2 ||
            screenPos.y > screenSize.height / 2
        );
    }
}
