import {BulletType} from "./BulletManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Bullet extends cc.Component {
    @property
    speed: number = 100;

    direction: cc.Vec2 = cc.Vec2.ZERO;
    bulletType: BulletType = null;

    init(position: cc.Vec2, direction: cc.Vec2, speed: number, bulletType: BulletType, angle: number) {
        this.node.x = position.x;
        this.node.y = position.y;
        this.direction = direction;
        this.speed = speed;
        this.bulletType = bulletType;
        this.node.angle = angle;
        
    }

    updateBullet(dt: number) {
        let moveStep = this.direction.mul(this.speed * dt);
        this.node.x += moveStep.x;
        this.node.y += moveStep.y;
        
    }
    onBeginContact(contact, self, other) {
        contact.disabled = true;
    }
}
